<?php
/**
 * Created by IntelliJ IDEA.
 * User: namax
 * Date: 17/10/16
 * Time: 04:24 PM
 */

namespace Helpers;

class Timing extends AbstractHelper
{
    static private $start = 0;

    public function start()
    {
        self::$start = microtime(true);
    }

    public function end()
    {
        if (self::$start == 0) {
            throw new Exception("Timing was not start");
        }
        return round((microtime(true) - self::$start), 4);
    }
}

