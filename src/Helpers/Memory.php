<?php
/**
 * Created by IntelliJ IDEA.
 * User: namax
 * Date: 17/10/16
 * Time: 04:25 PM
 */

namespace Helpers;

class Memory extends AbstractHelper
{
    public function show()
    {
        $size = memory_get_usage(true);
        $unit = ['b', 'kb', 'mb', 'gb', 'tb', 'pb'];
        return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
    }
}