<?php

/**
 * Created by IntelliJ IDEA.
 * User: namax
 * Date: 17/10/16
 * Time: 04:23 PM
 */

namespace Helpers;

/**
 * Description of AbstractHelper
 *
 * @author namax
 */
class AbstractHelper
{

    public static function create()
    {
        return new static();
    }

}