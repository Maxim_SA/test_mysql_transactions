<?php
system("clear");

mb_internal_encoding('UTF-8');
error_reporting(E_ALL);
ini_set('display_errors', '1');
set_time_limit(0);

defined('ROOT_PATH') || define('ROOT_PATH', realpath(__DIR__));
defined('DS') || define('DS', DIRECTORY_SEPARATOR);

require_once ROOT_PATH . '/vendor/autoload.php';

define('DB_HOST', 'localhost');
define('DB_PORT', 3306);
define('DB_USER', 'root');
define('DB_DATABASE', 'sam_dev');
define('DB_PASS', '1');
define('TRANSACTIONS_COUNTER', 10000);

function main()
{

    $db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_DATABASE, DB_PORT);

    if (!$db) {
        echo "Error: Unable to connect to MySQL." . PHP_EOL;
        echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
        echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
        exit;
    }

    echo "Host information: " . mysqli_get_host_info($db) . PHP_EOL;

    for ($i = 0; $i < TRANSACTIONS_COUNTER; $i++) {
        echo "TRANSACTION : #" . $i . PHP_EOL;
        testSelectForUpdateLock($db);
        echo "----------------------------------------"  . PHP_EOL;
    }

    mysqli_close($db);
}

/**
 * @param $db
 */
function testSelectForUpdateLock($db)
{
    $db->query("SET AUTOCOMMIT=0;");
    $result = $db->query("SELECT counter FROM  `test_transaction`   WHERE  `id`=1 FOR UPDATE;");

    if ($result) {
        // printf("Select returned %d rows.\n", mysqli_num_rows($result));
        //
        // while ($row = $result->fetch_row()) {
        //     printf("Counter (%s)\n", $row[0]);
        // }

        //mysqli_free_result($result);
    }

    $db->query("UPDATE `test_transaction` SET `counter`=`counter`+1,  `date`= now()  WHERE  `id`=1  ;");
    echo "Stop transaction" . PHP_EOL;
    $db->query("COMMIT;");
    $db->query("SET AUTOCOMMIT=1;");
}

echo "Memory on start: " . \Helpers\Memory::create()->show() . PHP_EOL;
\Helpers\Timing::create()->start() . PHP_EOL;
main();
echo "Time: " . \Helpers\Timing::create()->end() . PHP_EOL;
echo "Memory Used: " . \Helpers\Memory::create()->show() . PHP_EOL;
