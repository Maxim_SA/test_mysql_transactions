CREATE TABLE `test_transaction` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `counter` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `date` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
)
  COLLATE='utf8_general_ci'
  ENGINE=InnoDB
;
